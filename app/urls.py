from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    #URL PRINCIPALES
    path('', views.Index, name='index'), 
    #Inicio de sesión
    path('login/',views.Form_login, name = 'form_login'),
    path('inicio_sesion/',views.Inicio_sesion, name = 'inicio_sesion'),
    #Cerrar sesión
    path('logout/', views.Cerrar_sesion, name='cerrar_sesion'),
    #Perfiles
    path('vista_profesor/', views.Vista_profesor, name='vista_profesor'),
    path('vista_estudiante/', views.Vista_estudiante, name = 'vista_estudiante'),
    path('Vista_administrador/', views.Vista_administrador, name='Vista_administrador'),

    #URL JUAN PABLO
    path('Ver_lista_grupos/', views.Ver_lista_grupos, name = 'Ver_lista_grupos'),
    path('Ver_grupo_profesor/<str:codigo>/', views.Ver_grupo_profesor, name = 'Ver_grupo_profesor'),

    path('Crear_actividad_form/<str:codigo>/', views.Crear_actividad_form, name='Crear_actividad_form'),
    path('Crear_actividad_profesor/<str:codigo>/', views.Crear_actividad_profesor, name = 'Crear_actividad_profesor'),

    path('Ver_Estudiante_profesor/<int:id>/', views.Ver_Estudiante_profesor, name = 'Ver_Estudiante_profesor'),
    path('Ver_Actividad_Profesor/<int:id>/', views.Ver_Actividad_Profesor, name = 'Ver_Actividad_Profesor'),

    path('Editar_actividad/<str:actividad>/', views.Editar_actividad_form, name='Editar_actividad_form'),
    path('Editar_actividad_profesor/<str:actividad>/', views.Editar_actividad_profesor, name = 'Editar_actividad_profesor'),

    path('Calificar_actividad_form/<int:id>/', views.Calificar_actividad_form, name='Calificar_actividad_form'),
    path('Calificar_Actividad_Profesor/<int:id>/', views.Calificar_Actividad_Profesor, name = 'Calificar_Actividad_Profesor'),



#URL FELIPE
    # ESTUDIANTE
    path('Ver_Lista_de_Grupos_Estudiante/', views.Ver_Lista_de_Grupos_Estudiante, name = 'Ver_Lista_de_Grupos_Estudiante'),
    path('Ver_grupo_Estudiante/<int:id_estudiantegrupo>/',views.Ver_Grupo_Estudiante, name = 'Ver_Grupo_Estudiante'),

    #VISTA ESTUDIANTE (ADMINISTRADOR)
    path('Lista_estudiante_Admin/', views.Lista_estudiante_Admin, name='Lista_estudiante_Admin'), 
    path('Crear_estudiante_admin_form/', views.form_crear_estudiante, name='form_crear_estudiante'),
    path('Crear_estudiante_admin/', views.Crear_estudiante_admin, name='Crear_estudiante_admin'),
    path('form_editar_Estudiante/<str:nombre>/', views.form_editar_Estudiante, name='form_editar_Estudiante'),
    path('Editar_Estudiante/<int:id>/', views.Editar_Estudiante, name='Editar_Estudiante'),
    
#URL MÓNICA

    #URL VISTA PROFESOR
    path('Ver_lista_Grupos_Admin/', views.Ver_lista_Grupos_Admin, name='Ver_lista_Grupos_Admin'),

    path('eliminargrupo/<int:id>/', views.eliminargrupo, name='eliminargrupo'),
    path('eliminar_grupo_post/<int:id>/', views.eliminar_grupo_post, name='eliminar_grupo_post'),

    path('verlistadeprofes/', views.verlistadeprofes, name='verlistadeprofes'),
    
    #URL LISTA DE GRUPOS
    path('Vergrupoadmini/<str:codigo>/', views.Vergrupoadmini, name='Vergrupoadmini'),
    path('eliminarestudiante/<int:id>/', views.eliminarestudiante, name='eliminarestudiante'),
    path('eliminar_estudiante_post/<int:id>/', views.eliminar_estudiante_post, name='eliminar_estudiante_post'),

    path('form_agregar_grupo/', views.form_agregar_grupo, name='form_agregar_grupo'),
    path('agregar_grupo/', views.agregar_grupo, name='agregar_grupo'),

    path('Editargrupo/<str:codigo>/', views.form_Editargrupoadmi, name='form_Editargrupoadmi'),
    path('Editargrupoadmi/<str:codigo>/', views.Editargrupoadmi, name='Editargrupoadmi'),

    path('Agregarestudiantenuevo/<str:codigo>/', views.form_AgregarEstudiante, name='form_AgregarEstudiante'),
    path('AgregarEstudiante/<int:id>/', views.AgregarEstudiante, name='AgregarEstudiante'),

    #URL VER LISTA DE PROFESORES
    path('verlistadeprofes/', views.verlistadeprofes, name='verlistadeprofes'),
    path('Verprofesoradmi/<int:id>/', views.Verprofesoradmi, name='Verprofesoradmi'),

    path('formCrearprofesor/', views.form_crearprofesor, name='form_crearprofesor'),
    path('Crearprofesor/', views.Crearprofesor, name='Crearprofesor'),
    
]
