from django.db import models
from django.contrib.auth.models import User
from django.db.models.base import Model
from django.db.models.fields.related import ForeignKey

class Profesor(models.Model):

    user=models.OneToOneField(User,related_name = 'profesor', null = False, on_delete = models.CASCADE)
    catedra=models.BooleanField(null = False, default = False)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    class Meta:
        app_label = 'app'

class Estudiante(models.Model):
    user = models.OneToOneField(User, related_name = 'estudiante', null = False, on_delete = models.CASCADE)
    codigo = models.CharField(max_length = 20, null = False)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    class Meta:
        app_label = 'app'

class Grupo(models.Model):
    codigo = models.CharField(max_length = 50, null = False)
    asignatura = models.CharField(max_length = 100, null = False)
    semestre = models.CharField(max_length = 20, null = False)
    profesor = models.ForeignKey(Profesor, related_name = "grupos", null = False, on_delete = models.PROTECT) 
    
    def __str__(self):
        return f'{self.asignatura}'

    class Meta:
        app_label = 'app'

 
class EstudianteGrupo(models.Model):
    grupo = models.ForeignKey(Grupo, related_name = 'estudiantesgrupo', null = False, on_delete = models.PROTECT)
    estudiante = models.ForeignKey(Estudiante, related_name = 'gruposestudiante', null = False, on_delete = models.PROTECT)

    def __str__(self):
        return f'{self.estudiante.user.first_name} {self.estudiante.user.last_name} {self.grupo}'

    class Meta:
        app_label = 'app'

class Actividad(models.Model):
    descripcion = models.CharField(max_length = 100, null = False)
    grupo = models.ForeignKey(Grupo, related_name = 'actividades',null = False, on_delete = models.CASCADE)
 
    def __str__(self):
        return self.descripcion

    class Meta: 
        app_label = 'app'

class Nota(models.Model):
    estudiante_grupo = models.ForeignKey(EstudianteGrupo, null = False, related_name = 'notasestudiante', on_delete = models.CASCADE)
    actividad = models.ForeignKey(Actividad, related_name = 'notasactividad', null=False, on_delete = models.CASCADE)
    valor = models.FloatField(null = False)

    def __str__(self):
        return f'{self.actividad} {self.valor}'

    class Meta:
        app_label = 'app'
