from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models.fields import EmailField
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from app.models import Profesor, Estudiante, Grupo, EstudianteGrupo, Actividad, Nota


def Index(request):
    return render(request, 'app/index.html')

def Form_login(request):
    return render(request, 'app/index.html')

def Inicio_sesion(request):
    profesores = Profesor.objects.all()
    usuario = request.POST['username']
    password = request.POST['password']
    user = authenticate(username = usuario, password = password)

    if user is not None:
        if user.is_superuser == True:
            login(request, user)
            return redirect('app:Vista_administrador')
        else:
            login(request, user)
            for profesor in profesores:
                print(user.id)
                print(profesor.id)
                print(profesor.user.id)
                if profesor.user.id == user.id:
                    return redirect('app:vista_profesor')
            return redirect('app:vista_estudiante')
    else:
        contexto = {
            'error':'El usuario no existe.'
        }
        print(user)
        return render(request, 'app/index.html', contexto)

def Cerrar_sesion(request):
    logout(request)
    return redirect('app:index')

@login_required
def Vista_profesor(request):
    profesor = request.user
    contexto = {
        'profesor' : profesor
    }
    return render(request, 'app/vista_profesor.html', contexto)
@login_required
def Vista_estudiante(request):
    estudiante = request.user
    contexto = {
        'estudiante' : estudiante
    }

    return render(request, 'app/perfil_estudiante.html',contexto)

@login_required
def Vista_administrador(request):
    administrador = request.user
    contexto ={
        'adminin': administrador
    }
    return render(request, 'app/Vista_administrador.html', contexto)


#VIEWS JUAN PABLO

def Ver_lista_grupos(request):
    profesor = request.user
    grupos = Grupo.objects.filter(profesor = profesor.profesor.id)
    contexto = {
        'materias': grupos
    }
    return render(request, 'app/19.1 Lista de Grupos Profesor.html', contexto)

def Ver_grupo_profesor(request, codigo):
    grupo = Grupo.objects.get(codigo = codigo)

    contexto = {
        'grupo': grupo
    }
    return render(request, 'app/20.1 Ver Grupo Profesor.html', contexto)

    
def Ver_Estudiante_profesor(request, id):
    estudiantes = EstudianteGrupo.objects.get(id = id)  

    contexto = {
        'estudiantes':estudiantes,
    }
    return render(request, 'app/21.1 Ver Estudiante profesor.html', contexto)


def Ver_Actividad_Profesor(request, id):
    actividad = Actividad.objects.get(id=id)

    contexto = {
        'actividad':actividad
    }    
    return render(request, 'app/17.1 Ver Actividad profesor.html', contexto)

def Editar_actividad_form(request, actividad):
    actividad = Actividad.objects.get(descripcion = actividad)
    print(actividad.descripcion)
    contexto = {
        'actividad': actividad
    }
    return render(request, 'app/16.1 Editar actividad profesor.html', contexto)


def Editar_actividad_profesor(request, actividad):

    actividad = Actividad.objects.get(descripcion = actividad)
    
    actividad.descripcion = request.POST['descripcion']
    actividad.save()

    return redirect('app:Ver_lista_grupos')

def Calificar_actividad_form(request, id):
    nota = Nota.objects.get(id = id)
    print(nota)
    contexto = {
        'nota': nota
    }
    return render(request, 'app/18.1 Calificar Actividad Profesor.html', contexto) 

def Calificar_Actividad_Profesor(request, id):
    calificacion = Nota.objects.get(id = id)
    nota = request.POST['nota']

    calificacion.valor = nota
    calificacion.save()
    return redirect('app:Ver_lista_grupos')

def Crear_actividad_form(request, codigo):
    grupo = Grupo.objects.get(codigo = codigo)
    contexto = {
        'grupo': grupo
    }
    return render(request, 'app/15 Crear actividad profesor.html', contexto)

def Crear_actividad_profesor(request, codigo):
    grupo = Grupo.objects.get(codigo = codigo)
    actividad = request.POST['actividad']

    nueva_actividad = Actividad()
    nueva_actividad.descripcion = actividad
    nueva_actividad.grupo = grupo
    nueva_actividad.save()
    
    return redirect('app:Ver_lista_grupos')

    


#VIEWS FELIPE
# ESTUDIANTE
def Ver_Lista_de_Grupos_Estudiante(request):
    estudiante = request.user
    print(estudiante)
    grupos = EstudianteGrupo.objects.filter(estudiante = estudiante.estudiante.id)
    print(grupos)
    contexto = {
        'grupos': grupos
    }
    return render(request, 'app/22_Ver_Lista_de_Grupos_Estudiante.html', contexto)

def Ver_Grupo_Estudiante(request, id_estudiantegrupo):

    estudiantegrupo = EstudianteGrupo.objects.get(id = id_estudiantegrupo)
    contexto = {
        'estudiantegrupo': estudiantegrupo
    }
    return render(request, 'app/23_Ver_Grupo_Estudiante.html',contexto) 

# VISTA DE ESTUDIANTE (ADMINISTRADOR)

def Lista_estudiante_Admin(request):
    estudiantes = Estudiante.objects.all()
    contexto = {
        'estudiantes': estudiantes
    }
    return render(request, 'app/13_Lista_estudiante_Admin.html', contexto)

def form_crear_estudiante(request):
    return render(request,'app/12_Crear_estudiante_admin.html')

def Crear_estudiante_admin(request):
    estudiantes=Estudiante.objects.all()

    apellidos = request.POST['apellido']
    nombres = request.POST['nombres']
    usuario = request.POST['usuario']
    email = request.POST['email']
    codigo = request.POST['codigo']
    password = request.POST['password']


    for estudiante in estudiantes:
        if estudiante.user.email == email:
            contexto = {
                'error':'El estudiante ya existe',
            }
            return render(request, 'app/12_Crear_estudiante_admin.html', contexto)

    user = User()
    user.username = usuario
    user.first_name = nombres
    user.last_name = apellidos
    user.first_name = nombres
    user.email = email
    user.set_password(password)


    user.save()

    estudiante = Estudiante()
    estudiante.user = user
    estudiante.codigo = codigo
    estudiante.save()

    return redirect('app:Lista_estudiante_Admin')

def form_editar_Estudiante(request, nombre):
    estudiante = User.objects.get(first_name = nombre)
    contexto = {
        'estudiante':estudiante
    }
    return render(request, 'app/14 Editar Estudiante.html',contexto)

def Editar_Estudiante(request,id):
    usuario = User.objects.get(id = id)
    estudiante = Estudiante.objects.get(user = usuario)
    estudiantes=Estudiante.objects.all()

    codigo = request.POST['codigo']
    apellidos = request.POST['apellidos']
    nombre = request.POST['nombre']
    username = request.POST['username']
    email = request.POST['email']

    # for estudiante in estudiantes:
    #     if estudiante.user.email == email:
    #         contexto = {
    #             'estudiante':usuario,
    #             'error':'El estudiante ya existe',
    #         }
    #         return render(request, 'app/14 Editar Estudiante.html', contexto)

    estudiante.codigo = codigo
    usuario.last_name = apellidos
    usuario.first_name = nombre
    usuario.username = username
    usuario.email = email
    usuario.save()
    estudiante.save()
    
    return redirect('app:Lista_estudiante_Admin')


#VIEWS MÓNICA

#Dirección vista administrador

def Ver_lista_Grupos_Admin(request):
    lista_grupos=Grupo.objects.all()
    estudiantes=EstudianteGrupo.objects.all()
    
    contexto = {
        'grupos': lista_grupos,
        'estudiantes':estudiantes
    }
    return render(request, 'app/5_Ver_lista_Grupos_Admin.html', contexto)

def eliminargrupo(request,id):
    grupo=Grupo.objects.get(id=id)
    contexto={
        'grupo':grupo
    }
    return render(request, 'app/51eliminargrupo.html',contexto)

def eliminar_grupo_post(request, id):
    grupo=Grupo.objects.get(id=id)
 
    if len(grupo.estudiantesgrupo.all()) == 0:
        grupo.delete()
        return redirect('app:Ver_lista_Grupos_Admin')
    else:
        contexto={
            'grupo':grupo,
            'mensaje':'No se puede eliminar el grupo, hay estudiantes inscritos.'
        }
        return render(request, 'app/51eliminargrupo.html', contexto)

def verlistadeprofes(request):
    lista_profesores=Profesor.objects.all()
    contexto = {
        'profesores': lista_profesores
    }
    return render(request, 'app/10_verlistadeprofes.html', contexto)

#Direcciones ver grupos
def Vergrupoadmini(request, codigo):
    grupo=Grupo.objects.get(codigo=codigo)
    estudiantes=EstudianteGrupo.objects.filter(grupo=grupo)
    contexto={
        'group':grupo,
        'estudiantes':estudiantes
    }
    return render(request, 'app/6Vergrupoadmini.html', contexto)

def eliminarestudiante(request,id):
    estudiante=EstudianteGrupo.objects.get(id=id)
    contexto={
        'estudiante':estudiante
    }
    return render(request, 'app/61eliminarestudiante.html',contexto)

def eliminar_estudiante_post(request, id):
    estudiante=EstudianteGrupo.objects.get(id=id)
    estudiante.delete()
    return redirect('app:Ver_lista_Grupos_Admin')
   
def form_Editargrupoadmi(request, codigo):
    grupo=Grupo.objects.get(codigo=codigo)
    contexto={
        'group':grupo
    }
    return render(request, 'app/4Editargrupoadmi.html', contexto)

def Editargrupoadmi(request,codigo):
    grupo=Grupo.objects.get(codigo=codigo)
    email_profesor=User.objects.get(id=grupo.profesor.user.id)
    print(email_profesor)

    #ATRAPA DATOS PARA CAMBIO
    codigo=request.POST['codigo']
    asignatura=request.POST['asignatura']
    semestre=request.POST['semestre']
    email=request.POST['email']

    grupo.codigo=codigo
    grupo.asignatura=asignatura
    grupo.semestre=semestre
    email_profesor.email=email
    grupo.save()
    email_profesor.save()

    return redirect('app:Ver_lista_Grupos_Admin')

def form_agregar_grupo(request):
    profesores=Profesor.objects.all()

    contexto={
        "profesores":profesores
    }

    return render(request, 'app/3Creargrupo.html', contexto)

def agregar_grupo(request):
    codigo=request.POST['codigo']
    asignatura=request.POST['asignatura']
    semestre=request.POST['semestre']
    profesor=int(request.POST['profesor'])

    profe=Profesor.objects.get(id=profesor)
    print(profe)

    grupo=Grupo()
    grupo.codigo=codigo
    grupo.asignatura=asignatura
    grupo.semestre=semestre
    grupo.profesor=profe
    grupo.save()

    return redirect('app:Ver_lista_Grupos_Admin')


def form_AgregarEstudiante(request, codigo):
    grupo=Grupo.objects.get(codigo=codigo)
    contexto={
        'group':grupo
    }
    return render(request, 'app/8AgregarEstudiante.html', contexto)

def AgregarEstudiante(request, id):
    grupo=Grupo.objects.get(id=id)
    cod_estudiante=request.POST.get('codigo')
    tabla=EstudianteGrupo.objects.filter(grupo=id)

    try:
        estudiante=Estudiante.objects.get(codigo=cod_estudiante)
    except: 
        contexto={
            'error':'El estudiante no existe en la institución',
            'group':grupo
        }
        return render(request, 'app/8AgregarEstudiante.html', contexto)

    for repite in tabla.all():        
        if repite.estudiante.codigo==cod_estudiante:
            contexto={
                'error':'El estudiante ya existe en el grupo',
                'group':grupo
            }
            return render(request, 'app/8AgregarEstudiante.html', contexto)

    nuevo=EstudianteGrupo()
    nuevo.estudiante=estudiante
    nuevo.grupo=grupo
    nuevo.save()

    return redirect('app:Ver_lista_Grupos_Admin')
    
#Direcciones Ver listas de profesores
def Verprofesoradmi(request,id):
    profesor=Profesor.objects.get(id=id)
    grupo=Grupo.objects.filter(profesor=profesor).order_by('semestre')

    contexto={
        'profesor':profesor,
        'grupo':grupo
    }

    return render(request, 'app/11_Verprofesoradmi.html', contexto)

def form_crearprofesor(request):

    return render(request, 'app/9_Crearprofesor.html')

def Crearprofesor(request):
    usuarion=User()
    
    profesores=Profesor.objects.all()

    usuarion.username=request.POST['username']
    usuarion.last_name=request.POST['apellidos']
    usuarion.first_name=request.POST['nombres']
    email=request.POST['email']
    usuarion.set_password(request.POST['contraseña'])
    

    for profesor in profesores:
        if profesor.user.email == email:
            contexto = {
                'error':'El profesor ya existe',
            }
            return render(request, 'app/9_Crearprofesor.html', contexto) 
    
    usuarion.email=email
    usuarion.save()
    nuevoprofe=Profesor()
    nuevoprofe.user=usuarion
    nuevoprofe.catedra=request.POST['catedra']
    nuevoprofe.save()

    return redirect('app:verlistadeprofes')